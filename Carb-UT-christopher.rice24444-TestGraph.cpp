// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
    graph_types =
    Types<
            boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
            Graph // uncomment, add a comma to the line above
            >;

#ifdef __APPLE__
    TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
    TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);}


// ---------------------------------------------------------------------------------
// Student-defined tests
// ---------------------------------------------------------------------------------

/*
	tests will primarily serve to check the operation of a particular function
	tests for functions within similar categories will be adjacent
	tests near the top will generally be simpler than those below
	generally, we attempt to test the feasible edge or exception cases for the functions
*/

// ---------------------------------------------------------------------------------
// add functions tests
// ---------------------------------------------------------------------------------

// ------
// add_vertex tests
// ------

TYPED_TEST(GraphFixture, add_vertex_basic) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertices_size_type vs1 = num_vertices(g);

    vertex_descriptor vdB = add_vertex(g);
    vertices_size_type vs2 = num_vertices(g);

    ASSERT_EQ(vs1, 1u);
	ASSERT_EQ(vs2, 2u);}

TYPED_TEST(GraphFixture, add_vertex_basic_for_loop) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    for (int i = 0; i < 10; i++) {
        add_vertex(g);
    }

    vertices_size_type vs = num_vertices(g);

	ASSERT_EQ(vs, 10u);}

TYPED_TEST(GraphFixture, add_vertex_basic_even) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertices_size_type vs1 = num_vertices(g);
    ASSERT_NE(vdA, vdB);

    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertices_size_type vs2 = num_vertices(g);
    ASSERT_NE(vdC, vdD);

    ASSERT_EQ(vs1, 2u);
	ASSERT_EQ(vs2, 4u);}

TYPED_TEST(GraphFixture, add_vertex_basic_odd) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertices_size_type vs1 = num_vertices(g);
    ASSERT_EQ(vs1, 1u);

    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertices_size_type vs2 = num_vertices(g);
    ASSERT_NE(vdB, vdC);
    ASSERT_EQ(vs2, 3u);

    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertices_size_type vs3 = num_vertices(g);
    ASSERT_NE(vdD, vdE);
    ASSERT_EQ(vs3, 5u);}


// ------
// add_edge tests
// ------

TYPED_TEST(GraphFixture, add_edge_basic_loop) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;

     vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 3u);
	ASSERT_EQ(es, 3u);}

TYPED_TEST(GraphFixture, add_edge_basic_web) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edAD = add_edge(vdA, vdD, g).first;

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 4u);
	ASSERT_EQ(es, 3u);}

TYPED_TEST(GraphFixture, add_edge_self_loop) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdA, g).first;

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 1u);
	ASSERT_EQ(es, 1u);}

TYPED_TEST(GraphFixture, add_edge_self_loop_multiple) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    edge_descriptor edAA = add_edge(vdA, vdA, g).first;
    edge_descriptor edBB = add_edge(vdB, vdB, g).first;
    edge_descriptor edCC = add_edge(vdC, vdC, g).first;

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 3u);
	ASSERT_EQ(es, 3u);}

TYPED_TEST(GraphFixture, add_edge_self_loop_auto) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;
    
    vertex_descriptor vdA;

    edge_descriptor edAB = add_edge(vdA, vdA, g).first;

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 1u);
	ASSERT_EQ(es, 1u);}

TYPED_TEST(GraphFixture, add_edge_duplicate) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    bool added           = add_edge(vdA, vdB, g).second;

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 2u);
    ASSERT_EQ(es, 1u);
    ASSERT_EQ(added, false);}

TYPED_TEST(GraphFixture, add_edge_duplicate_multiple) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    for (int i = 0; i < 10; i++) {
        add_edge(vdA, vdB, g);
    }

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 2u);
    ASSERT_EQ(es, 1u);}


// // ---------------------------------------------------------------------------------
// // edge information functions tests
// // ---------------------------------------------------------------------------------

// // ------
// // edge tests
// // ------

TYPED_TEST(GraphFixture, edge_basic) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    std::pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);

    ASSERT_EQ(e1.second, true);}

TYPED_TEST(GraphFixture, edge_basic_multiple) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edAD = add_edge(vdA, vdD, g).first;

    std::pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);
    std::pair<edge_descriptor, bool> e2 = edge(vdA, vdC, g);
    std::pair<edge_descriptor, bool> e3 = edge(vdA, vdD, g);

    ASSERT_EQ(e1.second, true);
    ASSERT_EQ(e2.second, true);
    ASSERT_EQ(e3.second, true);}

TYPED_TEST(GraphFixture, edge_basic_cycle_loop) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edCB = add_edge(vdC, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    std::pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);
    std::pair<edge_descriptor, bool> e2 = edge(vdB, vdC, g);
    std::pair<edge_descriptor, bool> e3 = edge(vdC, vdA, g);
    std::pair<edge_descriptor, bool> e4 = edge(vdB, vdA, g);
    std::pair<edge_descriptor, bool> e5 = edge(vdC, vdB, g);
    std::pair<edge_descriptor, bool> e6 = edge(vdA, vdC, g);

    ASSERT_EQ(e1.second, true);
    ASSERT_EQ(e2.second, true);
    ASSERT_EQ(e3.second, true);
    ASSERT_EQ(e4.second, true);
    ASSERT_EQ(e5.second, true);
    ASSERT_EQ(e6.second, true);}

TYPED_TEST(GraphFixture, edge_not_present) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    
    std::pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);

    ASSERT_EQ(e1.second, false);}

TYPED_TEST(GraphFixture, edge_change) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    std::pair<edge_descriptor, bool> e1 = edge(vdA, vdB, g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    std::pair<edge_descriptor, bool> e2 = edge(vdA, vdB, g);

    ASSERT_EQ(e1.second, false);
    ASSERT_EQ(e2.second, true);}

// // ------
// // edges tests
// // ------

TYPED_TEST(GraphFixture, edges_basic) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator       = typename graph_type::edge_iterator;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edABdupe = add_edge(vdA, vdB, g).first;

    edges_size_type es = num_edges(g);

    pair<edge_iterator, edge_iterator> p = edges(g);

    edge_iterator                      b = p.first;

    ASSERT_EQ(edAB, *b);
    ASSERT_EQ(edABdupe, *b);
    ASSERT_EQ(es, 1u);}

TYPED_TEST(GraphFixture, edges_basic_2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edge_iterator       = typename graph_type::edge_iterator;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;

    edges_size_type es = num_edges(g);

    pair<edge_iterator, edge_iterator> p = edges(g);

    edge_iterator                      b = p.first;

    ASSERT_EQ(edAB, *b);
    ++b;
    ASSERT_EQ(edBA, *b);
    ++b;
    ASSERT_EQ(edBC, *b);
    ++b;
    ASSERT_EQ(edCA, *b);
    ++b;
    ASSERT_EQ(p.second, b);

    ASSERT_EQ(es, 4u);}

TYPED_TEST(GraphFixture, edges_count_basic) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    
    edges_size_type es1 = 0;

    edge_iterator ei1 = p.first;
    while(ei1 != p.second) {
        ++es1;
        ++ei1;
    }


    edges_size_type es2 = num_edges(g);

    ASSERT_EQ(es1, es2);}

TYPED_TEST(GraphFixture, edges_count_dupes) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;

    edge_descriptor edAB2 = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC2 = add_edge(vdB, vdC, g).first;
    edge_descriptor edCA2 = add_edge(vdC, vdA, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    
    edges_size_type es1 = 0;

    edge_iterator ei1 = p.first;
    while(ei1 != p.second) {
        ++es1;
        ++ei1;
    }


    edges_size_type es2 = num_edges(g);

    ASSERT_EQ(es1, es2);}

TYPED_TEST(GraphFixture, edges_count_none) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;
    using edge_iterator       = typename graph_type::edge_iterator;

    graph_type g;

    pair<edge_iterator, edge_iterator> p = edges(g);
    
    edges_size_type es1 = 0;

    edge_iterator ei1 = p.first;
    while(ei1 != p.second) {
        ++es1;
        ++ei1;
    }


    edges_size_type es2 = num_edges(g);

    ASSERT_EQ(es1, es2);}

// // ------
// // num_edges tests
// // ------

TYPED_TEST(GraphFixture, num_edges_dupes) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;

    //trying to add an edge that already exists should not alter anything
    edge_descriptor edAB2 = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC2 = add_edge(vdB, vdC, g).first;
    edge_descriptor edCA2 = add_edge(vdC, vdA, g).first;

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 3u);
	ASSERT_EQ(es, 3u);}

TYPED_TEST(GraphFixture, num_edges_loop) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    
    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edCB = add_edge(vdC, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 3u);
	ASSERT_EQ(es, 6u);}

TYPED_TEST(GraphFixture, num_edges_none) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 0u);
	ASSERT_EQ(es, 0u);}



// // ---------------------------------------------------------------------------------
// // vertex information functions tests
// // ---------------------------------------------------------------------------------

// // ------
// // vertex tests
// // ------

TYPED_TEST(GraphFixture, vertex_basic) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    vertex_descriptor vd1 = vertex(0,g);
    vertex_descriptor vd2 = vertex(1,g);
    vertex_descriptor vd3 = vertex(2,g);

    vertex_descriptor vdX = vertex(vdA,g);
    vertex_descriptor vdY = vertex(vdB,g);
    vertex_descriptor vdZ = vertex(vdC,g);

    ASSERT_EQ(vdA, 0);
    ASSERT_EQ(vdA, vd1);
    ASSERT_EQ(vdA, vdX);

    ASSERT_EQ(vdB, 1);
    ASSERT_EQ(vdB, vd2);
    ASSERT_EQ(vdB, vdY);

    ASSERT_EQ(vdC, 2);
    ASSERT_EQ(vdC, vd3);
    ASSERT_EQ(vdC, vdZ);
    
    ASSERT_NE(vd1, vdB);
    ASSERT_NE(vdZ, vdA);
    ASSERT_NE(vd3, vdY);}

TYPED_TEST(GraphFixture, vertex_bad_index) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    vertex_descriptor vd1 = vertex(6,g);

    ASSERT_NE(vdA, vd1);
    ASSERT_NE(vdB, vd1);
    ASSERT_NE(vdC, vd1);}


// // ------
// // vertices tests
// // ------

TYPED_TEST(GraphFixture, vertices_basic) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using vertex_iterator    = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    auto verts = vertices(g);

    vertex_iterator vi1 = verts.first;

    ASSERT_EQ(*vi1, vdA);
    ++vi1;

    ASSERT_EQ(*vi1, vdB);
    ++vi1;

    ASSERT_EQ(*vi1, vdC);
    ++vi1;

    ASSERT_EQ(*vi1, vdD);
    ++vi1;

    ASSERT_EQ(vi1, verts.second);}


TYPED_TEST(GraphFixture, vertices_count_basic) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    auto verts = vertices(g);
    vertices_size_type vs1 = *(verts.second) - *(verts.first);
    vertices_size_type vs2 = num_vertices(g);

    ASSERT_EQ(vs1, vs2);}

TYPED_TEST(GraphFixture, vertices_count_mult) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    auto verts = vertices(g);
    vertices_size_type vs1 = *(verts.second) - *(verts.first);
    vertices_size_type vs2 = num_vertices(g);

    ASSERT_EQ(vs1, vs2);}

TYPED_TEST(GraphFixture, vertices_count_empty) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    auto verts = vertices(g);
    vertices_size_type vs1 = *(verts.second) - *(verts.first);
    vertices_size_type vs2 = num_vertices(g);

    ASSERT_EQ(vs1, vs2);}


// // ------
// // num_vertices tests
// // ------

TYPED_TEST(GraphFixture, num_vertex_basic_large) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    for(int i = 0; i < 1000; i++) {
        add_vertex(g);
    }

    vertices_size_type vs = num_vertices(g);

    ASSERT_EQ(vs, 1000u);}

TYPED_TEST(GraphFixture, num_vertex_basic_small) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    for(int i = 0; i < 10; i++) {
        add_vertex(g);
    }

    vertices_size_type vs = num_vertices(g);

    ASSERT_EQ(vs, 10u);}

TYPED_TEST(GraphFixture, num_vertex_basic_none) {
    using graph_type         = typename TestFixture::graph_type;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertices_size_type vs = num_vertices(g);

    ASSERT_EQ(vs, 0u);}




// // ---------------------------------------------------------------------------------
// // graph information functions tests
// // ---------------------------------------------------------------------------------

// // ------
// // source tests
// // ------

TYPED_TEST(GraphFixture, source_basic) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    
    vertex_descriptor vd1 = source(edAB, g);
    vertex_descriptor vd2 = source(edAC, g);
    vertex_descriptor vd3 = source(edBA, g);
    vertex_descriptor vd4 = source(edCA, g);

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vd1, vdA);
    ASSERT_EQ(vd2, vdA);
    ASSERT_EQ(vd1, vd2);

    ASSERT_EQ(vd3, vdB);
    ASSERT_EQ(vd4, vdC);
    ASSERT_NE(vd3, vd4);

    ASSERT_EQ(vs, 3u);
	ASSERT_EQ(es, 4u);}

TYPED_TEST(GraphFixture, source_self_edge) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    edge_descriptor edAA = add_edge(vdA, vdA, g).first;
    
    vertex_descriptor vd1 = source(edAA, g);

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vd1, vdA);

    ASSERT_EQ(vs, 1u);
	ASSERT_EQ(es, 1u);}

// // ------
// // target tests
// // ------
	
TYPED_TEST(GraphFixture, target_basic) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    
    vertex_descriptor vd1 = target(edAB, g);
    vertex_descriptor vd2 = target(edAC, g);
    vertex_descriptor vd3 = target(edBA, g);
    vertex_descriptor vd4 = target(edCA, g);

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vd1, vdB);
    ASSERT_EQ(vd2, vdC);
    ASSERT_NE(vd1, vd2);

    ASSERT_EQ(vd3, vdA);
    ASSERT_EQ(vd4, vdA);
    ASSERT_EQ(vd3, vd4);

    ASSERT_EQ(vs, 3u);
	ASSERT_EQ(es, 4u);}

TYPED_TEST(GraphFixture, target_self_edge) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    edge_descriptor edAA = add_edge(vdA, vdA, g).first;
    
    vertex_descriptor vd1 = target(edAA, g);

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vd1, vdA);

    ASSERT_EQ(vs, 1u);
	ASSERT_EQ(es, 1u);}

// ------
// adjacent_vertices tests
// ------

TYPED_TEST(GraphFixture, adjacent_verticies_basic) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using adjacency_iterator    = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    auto adj = adjacent_vertices(vdA, g);
    adjacency_iterator ai1 = adj.first;

    ASSERT_EQ(*(ai1), vdB);

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 3u);
	ASSERT_EQ(es, 2u);}

TYPED_TEST(GraphFixture, adjacent_verticies_basic_2) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using adjacency_iterator    = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edAD = add_edge(vdA, vdD, g).first;

    auto adj = adjacent_vertices(vdA, g);
    adjacency_iterator ai1 = adj.first;

    ASSERT_EQ(*ai1, vdB);
    ++ai1;
    ASSERT_EQ(*ai1, vdC);
    ++ai1;
    ASSERT_EQ(*ai1, vdD);
    ++ai1;
    ASSERT_EQ(ai1, adj.second);

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 4u);
    ASSERT_EQ(es, 3u);}

TYPED_TEST(GraphFixture, adjacent_none) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;
    using adjacency_iterator    = typename TestFixture::adjacency_iterator;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using edges_size_type     = typename graph_type::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    auto adj = adjacent_vertices(vdA, g);
    adjacency_iterator ai1 = adj.first;

    ASSERT_EQ(ai1, adj.second);

    vertices_size_type vs = num_vertices(g);
    edges_size_type es = num_edges(g);

    ASSERT_EQ(vs, 2u);
    ASSERT_EQ(es, 0u);}

