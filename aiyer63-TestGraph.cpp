// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;
};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,Graph>;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);

    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

//Add existing edge
TYPED_TEST(GraphFixture, test5) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    //using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    pair<edge_descriptor, bool> p2 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first, p2.first);
    ASSERT_TRUE(p1.second);
    ASSERT_FALSE(p2.second);
}

//Test edges method
TYPED_TEST(GraphFixture, test6) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    //using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdA, vdD, g);
    add_edge(vdA, vdE, g);
    std::pair<edge_iterator, edge_iterator> p = edges(g);

    edge_iterator b = p.first;
    vertex_descriptor src = source(*b, g);
    //Use assert to get rid of maybe_unintialized warning
    //assert(b != p.second);
    int counter = 0;
    while(b != p.second) {
        src = source(*b, g);
        vertex_descriptor tgt = target(*b, g);
        ASSERT_EQ(vdA, src);
        ASSERT_NE(vdA, tgt);
        ++b;
        counter++;
    }
}

//Test edge method
TYPED_TEST(GraphFixture, test7) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    graph_type g;
    set<edge_descriptor> edSet;
    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    edSet.insert(add_edge(vdA, vdB, g).first);
    edSet.insert(add_edge(vdA, vdC, g).first);
    edSet.insert(add_edge(vdA, vdD, g).first);
    edSet.insert(add_edge(vdA, vdE, g).first);
    auto edSet_end = edSet.end();
    auto p = adjacent_vertices(vdA, g);
    adjacency_iterator b = p.first;
    while(b != p.second) {
        auto res1 = edge(vdA, *b, g);
        auto res2 = edge(*b, vdA, g);
        ASSERT_TRUE(res1.second);
        ASSERT_FALSE(res2.second);

        //Tests that all edge descriptors returned by edge() correspond to valid edges
        auto edSet_iter = edSet.find(res1.first);
        ASSERT_NE(edSet_iter, edSet_end);
        ++b;
    }
}

//  vertices and vertex iterator
TYPED_TEST(GraphFixture, test8) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vector<vertex_descriptor> vd_list;
    int counter = 20;
    while (counter) {
        vd_list.push_back(add_vertex(g));
        --counter;
    }
    ASSERT_EQ(num_edges(g), 0);
    ASSERT_EQ(num_vertices(g), 20);
    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator b = p.first;
    vertex_iterator e = p.second;
    auto b2 = vd_list.begin();
    auto e2 = vd_list.end();
    while(b!=e && b2!=e2) {
        ASSERT_EQ(*b,*b2);
        ++b;
        ++b2;
    }
}


//  vertex() normal use
TYPED_TEST(GraphFixture, test9) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vector<vertex_descriptor> vd_list;
    int counter = 20;
    while (counter) {
        vd_list.push_back(add_vertex(g));
        --counter;
    }
    ASSERT_EQ(num_edges(g), 0);
    ASSERT_EQ(num_vertices(g), 20);
    auto b = vd_list.begin();
    auto e = vd_list.end();
    while(counter != 20 && b != e) {
        ASSERT_EQ(vertex(counter, g), *b);
        ++b;
        ++counter;
    }
}


//  vertex() invalid use
TYPED_TEST(GraphFixture, test10) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    ASSERT_EQ(num_vertices(g), 0u);
    ASSERT_EQ(vertex(1u, g), 1u);
    ASSERT_EQ(vertex(24u, g), 24u);
}


//  edge() normal use
TYPED_TEST(GraphFixture, test11) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vector<vertex_descriptor> vds;
    int counter = 5;
    while (counter) {
        vds.push_back(add_vertex(g));
        --counter;
    }
    add_edge(vds.at(2), vds.at(1), g);
    add_edge(vds.at(0), vds.at(4), g);
    ASSERT_EQ(num_vertices(g), 5u);
    ASSERT_EQ(num_edges(g), 2);
    auto p1 = edge(vds.at(2), vds.at(1), g);
    auto p2 = edge(vds.at(0), vds.at(4), g);
    auto p3 = edge(vds.at(1), vds.at(2), g);
    ASSERT_TRUE(p1.second);
    ASSERT_TRUE(p2.second);
    ASSERT_FALSE(p3.second);
}


//Self loop
TYPED_TEST(GraphFixture, test12) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vector<vertex_descriptor> vds;
    int counter = 5;
    while (counter) {
        vds.push_back(add_vertex(g));
        --counter;
    }
    add_edge(vds.at(2), vds.at(2), g);
    ASSERT_EQ(num_vertices(g), 5u);
    ASSERT_EQ(num_edges(g), 1);
    auto p1 = edge(vds.at(2), vds.at(2), g);
    ASSERT_TRUE(p1.second);
    ASSERT_EQ(source(p1.first, g), vds.at(2));
    ASSERT_EQ(target(p1.first, g), vds.at(2));
}


//  edge() complete graph
TYPED_TEST(GraphFixture, test13) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vector<vertex_descriptor> vds;
    int counter = 5;
    while (counter) {
        vds.push_back(add_vertex(g));
        --counter;
    }
    for(auto src = vds.begin(); src != vds.end(); ++src) {
        for(auto tgt = vds.begin(); tgt != vds.end(); ++tgt) {
            add_edge(*src, *tgt, g);
        }
    }

    //25 not 20 because of self loops
    ASSERT_EQ(num_edges(g), 25u);
}


//  edge() complete graph part two electric boogaloo
TYPED_TEST(GraphFixture, test14) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vector<vertex_descriptor> vds;
    int counter = 7;
    while (counter) {
        vds.push_back(add_vertex(g));
        --counter;
    }
    for(auto src = vds.begin(); src != vds.end(); ++src) {
        for(auto tgt = vds.begin(); tgt != vds.end(); ++tgt) {
            add_edge(*src, *tgt, g);
        }
    }

    ASSERT_EQ(num_edges(g), 49u);
    for(auto src = vds.begin(); src != vds.end(); ++src) {
        for(auto tgt = vds.begin(); tgt != vds.end(); ++tgt) {
            ASSERT_TRUE(edge(*src, *tgt, g).second);
        }
    }
}


//  complete graph adjacency check
TYPED_TEST(GraphFixture, test15) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vector<vertex_descriptor> vds;
    int counter = 12;
    while (counter) {
        vds.push_back(add_vertex(g));
        --counter;
    }

    for(auto src = vds.begin(); src != vds.end(); ++src) {
        for(auto tgt = vds.begin(); tgt != vds.end(); ++tgt) {
            add_edge(*src, *tgt, g);
        }
    }

    ASSERT_EQ(num_edges(g), 144u);

    for(auto src = vds.begin(); src != vds.end(); ++src) {
        pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(*src, g);
        auto b = p.first;
        auto e = p.second;
        while (b != e) {
            bool match = false;
            auto b2 = vds.begin();
            auto e2 = vds.end();
            while(b2 != e2 && !match) {
                if(*b2 == *b) {
                    match = true;
                }
                ++b2;
            }
            ASSERT_TRUE(match);
            ++b;
        }
    }
}

//  add edge with non existant vertices
TYPED_TEST(GraphFixture, test16) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vector<vertex_descriptor> vds;
    int counter = 3;
    while (counter) {
        vds.push_back(add_vertex(g));
        --counter;
    }
    vertex_descriptor vd1 = vds.at(2) + 1;
    vertex_descriptor vd2 = vd1 + 2;
    pair<edge_descriptor, bool> p = add_edge(vd1, vd2, g);
    ASSERT_TRUE(p.second);
    ASSERT_EQ(num_edges(g), 1u);
    //vd2 should be 5
    ASSERT_EQ(num_vertices(g), 6u);
}


//  add edge with existing to non-existing vertex
TYPED_TEST(GraphFixture, test17) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vector<vertex_descriptor> vds;
    int counter = 3;
    while (counter) {
        vds.push_back(add_vertex(g));
        --counter;
    }
    vertex_descriptor vd1 = vds.at(0);
    vertex_descriptor vd2 = vd1 + 4;
    pair<edge_descriptor, bool> p = add_edge(vd1, vd2, g);
    ASSERT_TRUE(p.second);
    ASSERT_EQ(num_edges(g), 1u);
    //vd2 should be 4
    ASSERT_EQ(num_vertices(g), 5u);
    p = edge(vd1, vd2, g);
    ASSERT_TRUE(p.second);
}

//  Consistency of vds after adding vertices
TYPED_TEST(GraphFixture, test18) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    add_vertex(g);
    add_vertex(g);
    auto vds1 = vertices(g);
    ASSERT_EQ(num_vertices(g), 2u);
    for(int i = 0; i < 240; ++i) {
        add_vertex(g);
    }
    auto vds2 = vertices(g);
    ASSERT_EQ(vds1.first, vds2.first);
}

//  Edge() with non-existant edge
TYPED_TEST(GraphFixture, test19) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    auto vd1 = add_vertex(g);
    auto vd2 = add_vertex(g);
    auto p = edge(vd1, vd2, g);
    ASSERT_FALSE(p.second);
}


//  big number of vertices
TYPED_TEST(GraphFixture, test20) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    int counter = 1000;
    int counter2 = counter;
    while(counter) {
        add_vertex(g);
        counter--;
    }
    ASSERT_EQ(num_vertices(g), counter2);
}

//  big number of vertices linear increase
TYPED_TEST(GraphFixture, test21) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    int counter = 1000;
    int counter2 = counter;
    while(counter) {
        add_vertex(g);
        counter--;
    }
    ASSERT_EQ(num_vertices(g), counter2);
    auto vs = vertices(g);
    auto e = vs.second;
    --e;
    counter2--;
    ASSERT_EQ(*e, counter2);
}


//  bidirectional iterator
TYPED_TEST(GraphFixture, test22) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    int counter = 5;
    int counter2 = counter;
    while(counter) {
        add_vertex(g);
        counter--;
    }
    ASSERT_EQ(num_vertices(g), counter2);
    auto vs = vertices(g);
    auto e = vs.second;
    --e;
    --counter2;
    while(vs.first != e) {
        ASSERT_EQ(*e, counter2);
        --counter2;
        --e;
    }
}

//  big vertices big edges
TYPED_TEST(GraphFixture, test23) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    int counter = 1000;
    int counter2 = counter;
    while(counter) {
        add_vertex(g);
        counter--;
    }
    ASSERT_EQ(num_vertices(g), counter2);
    int edge_counter = 150;
    int edge_counter2 = edge_counter;
    auto vs = vertices(g);
    auto b = vs.first;
    while(edge_counter) {
        add_edge(*b, *++b, g);
        --edge_counter;
    }
    ASSERT_EQ(num_edges(g), edge_counter2);
}

//  two graphs dont influence each other
TYPED_TEST(GraphFixture, test24) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    add_vertex(g1);
    ASSERT_EQ(num_vertices(g1), 1);
    ASSERT_EQ(num_vertices(g2), 0);
}


//  two graphs dont influence each other
TYPED_TEST(GraphFixture, test25) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    add_vertex(g2);
    ASSERT_EQ(num_vertices(g2), 1);
    ASSERT_EQ(num_vertices(g1), 0);
}

//  two graphs dont influence each other
TYPED_TEST(GraphFixture, test26) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    add_vertex(g1);
    add_vertex(g2);
    add_edge(0,0,g2);
    ASSERT_EQ(num_edges(g2), 1);
    ASSERT_EQ(num_edges(g1), 0);
}

//  two graphs dont influence each other
TYPED_TEST(GraphFixture, test27) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    add_vertex(g1);
    add_vertex(g2);
    add_edge(0,0,g1);
    ASSERT_EQ(num_edges(g1), 1);
    ASSERT_EQ(num_edges(g2), 0);
}

//  two graphs dont influence each other
TYPED_TEST(GraphFixture, test28) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    add_vertex(g1);
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);
    add_edge(0,0,g1);
    ASSERT_EQ(num_edges(g1), 1);
    ASSERT_EQ(num_edges(g2), 0);
    ASSERT_EQ(num_vertices(g1), 1);
    ASSERT_EQ(num_vertices(g2), 4);
}

//  two graphs dont influence each other
TYPED_TEST(GraphFixture, test29) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    add_vertex(g1);
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);
    add_edge(0,0,g2);
    add_edge(0,1,g2);
    ASSERT_EQ(num_edges(g2), 2);
    ASSERT_EQ(num_edges(g1), 0);
    ASSERT_EQ(num_vertices(g1), 1);
    ASSERT_EQ(num_vertices(g2), 4);
}

//  two graphs dont influence each other
TYPED_TEST(GraphFixture, test30) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    add_vertex(g1);
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);
    add_edge(0,0,g2);
    add_edge(0,1,g2);
    auto g1_v = vertices(g1);
    auto g2_v = vertices(g2);
    //Boost for some reason has these both as nullptrs
    //ASSERT_NE(g1_v.first, g2_v.first)
    ASSERT_NE(g1_v.first, g2_v.second);
}

//  two graphs dont influence each other
TYPED_TEST(GraphFixture, test31) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    add_vertex(g1);
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);
    add_edge(0,0,g2);
    add_edge(0,1,g2);
    auto g1_e = edges(g1);
    auto g2_e = edges(g2);
    ASSERT_NE(g1_e.first, g2_e.second);
}

//  two graphs dont influence each other
TYPED_TEST(GraphFixture, test32) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    add_vertex(g1);
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);
    add_edge(0,0,g2);
    add_edge(0,1,g2);
    auto g1_e = edges(g1);
    auto g2_e = edges(g2);
    ASSERT_NE(g2_e.first, g1_e.second);
}

//  two graphs dont influence each other
TYPED_TEST(GraphFixture, test33) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    add_vertex(g1);
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);
    add_vertex(g2);
    add_edge(0,0,g2);
    add_edge(0,1,g2);
    auto g1_v = vertices(g1);
    auto g2_v = vertices(g2);
    ASSERT_NE(g2_v.first, g1_v.second);
}


//  two graphs dont influence each other
TYPED_TEST(GraphFixture, test34) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    add_vertex(g2);
    add_vertex(g1);
    add_vertex(g1);
    add_vertex(g1);
    add_vertex(g1);
    add_edge(0,0,g2);
    add_edge(0,1,g1);
    auto g1_v = vertices(g1);
    auto g2_v = vertices(g2);
    ASSERT_NE(g2_v.first, g1_v.second);
}


//  two graphs dont influence each other
TYPED_TEST(GraphFixture, test35) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g1;
    graph_type g2;
    add_vertex(g2);
    add_vertex(g1);
    add_vertex(g1);
    add_vertex(g1);
    add_vertex(g1);
    add_edge(0,0,g2);
    add_edge(0,1,g1);
    auto g1_e = edges(g1);
    auto g2_e = edges(g2);
    ASSERT_NE(g2_e.first, g1_e.second);
}

//Testing default graph
TYPED_TEST(GraphFixture, test36) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    ASSERT_EQ(num_edges(g), 0);
    ASSERT_EQ(num_vertices(g), 0);
    ASSERT_EQ(vertex(102, g), 102);
}


//Testing default graph adding edge
TYPED_TEST(GraphFixture, test37) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    add_edge(12, 20, g);
    ASSERT_EQ(num_edges(g), 1);
    ASSERT_EQ(num_vertices(g), 21);
}

//Testing adding edge from existing vertex to big non existent
TYPED_TEST(GraphFixture, test38) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    add_vertex(g);
    add_edge(0, 99, g);
    ASSERT_EQ(num_edges(g), 1);
    ASSERT_EQ(num_vertices(g), 100);
}

//Testing adding edge from existing vertex to big non existent and then adjacency
TYPED_TEST(GraphFixture, test39) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using edge_descriptor     = typename TestFixture::edge_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;
    using edge_iterator       = typename TestFixture::edge_iterator;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;
    add_vertex(g);
    add_edge(0, 99, g);
    ASSERT_EQ(num_edges(g), 1);
    ASSERT_EQ(num_vertices(g), 100);
    auto p = adjacent_vertices(0, g);
    ASSERT_EQ(*p.first, 99);
}


